package convert

import (
	"bytes"
	"fmt"
	"os"
	"path/filepath"
	"strings"
	"text/template"

	"git.sr.ht/~kota/adelaii.de/normalize"
	"git.sr.ht/~kota/adelaii.de/ui"
	"git.sr.ht/~kota/adelaii.de/visible"
	"git.sr.ht/~kota/adelaii.de/walk"
	"git.sr.ht/~kota/adelaii.de/wiki"
	"github.com/yuin/goldmark"
	"github.com/yuin/goldmark/extension"
)

type Page struct {
	Content   string
	PathBar   []Location
	Neighbors []Location
}

type Location struct {
	Display string
	URL     string
}

func (p Page) Store(path string) error {
	tmpl, err := template.ParseFS(ui.Files, "main.tmpl")
	if err != nil {
		return fmt.Errorf("failed to load main.tmpl: %v", err)
	}
	var buf bytes.Buffer
	err = tmpl.Execute(&buf, p)
	if err != nil {
		return fmt.Errorf(
			"failed while executing template on %s: %v",
			path,
			err,
		)
	}
	data := buf.Bytes()

	// Write file.
	if err := os.MkdirAll(filepath.Dir(path), 0755); err != nil {
		return err
	}
	return os.WriteFile(path, data, 0644)
}

// All processes a list of input files, cleaning up and converting each one to
// html and writing the final output into the outDir. The inDir portion of
// each filepath is replaced with outDir; so subdirectory structures remain
// unchanged.
func All(n *walk.Node, inDir, outDir string) error {
	for n != nil {
		fmt.Println(n.Path)
		if n.IsDir {
			n = n.Next()
			continue
		}
		if !strings.HasSuffix(n.Path, ".md") {
			// Copy unmofified for non-markdown files.
			if err := copyFile(n.Path, inDir, outDir); err != nil {
				return err
			}
			n = n.Next()
			continue
		}

		// Create page from markdown files.
		data, err := os.ReadFile(n.Path)
		if err != nil {
			return err
		}

		// Replace certain wiki-specific links with markdown links.
		data = wiki.ReplaceLinks(data)

		// Strip out the visible keyword.
		data = visible.Strip(data)

		// Convert to html.
		md := goldmark.New(
			goldmark.WithExtensions(
				extension.Linkify,
				extension.Strikethrough,
			),
		)
		var buf bytes.Buffer
		if err := md.Convert(data, &buf); err != nil {
			return fmt.Errorf("failed to convert markdown to html: %v", err)
		}

		path := outputPath(n.Path, inDir, outDir)
		neighbors, err := neighbors(n)
		if err != nil {
			return err
		}
		page := Page{
			Content:   buf.String(),
			PathBar:   pathBar(path, outDir),
			Neighbors: neighbors,
		}
		if err := page.Store(path); err != nil {
			return err
		}
		n = n.Next()
	}
	return nil
}

// pathBar builds a slice of locations from a path.
// It basically splits up the path into sections (using /) and sets the URL for
// each section. The outDir is trimmed from the beggining of the path for
// display purposes.
func pathBar(path, outDir string) []Location {
	// Trim off output directory prefix.
	path = strings.TrimPrefix(path, filepath.Clean(outDir)+"/")

	// Strip "index" off the end.
	path = strings.TrimSuffix(path, "index.html")

	sections := strings.SplitAfter(path, "/")
	locations := []Location{
		{
			Display: "~/",
			URL:     "/",
		},
	}
	var url bytes.Buffer
	url.WriteString("/")
	for _, s := range sections {
		url.WriteString(s)
		locations = append(locations, Location{
			Display: strings.TrimSuffix(s, ".html"),
			URL:     url.String(),
		},
		)
	}
	return locations
}

// outputPath returns the output path for a given input and output directory.
func outputPath(input, inDir, outDir string) string {
	path := strings.TrimPrefix(input, filepath.Clean(inDir))
	path = filepath.Join(outDir, path)
	path = strings.TrimSuffix(path, ".md")
	path = normalize.String(path + ".html")
	return path
}

// neighbors builds up a slice of neighboring files and direcories to a path.
func neighbors(n *walk.Node) ([]Location, error) {
	var neighbors []Location
	siblings := n.Siblings()
	for _, s := range siblings {
		if !s.IsVisible {
			continue
		}
		if s.IsDir {
			neighbors = append(neighbors, Location{
				Display: s.Name + "/",
				URL:     s.Name,
			})
			continue
		}
		if s.Name == "index.md" {
			// Skip index.md since we've already added the directory itself.
			continue
		}
		name := strings.TrimSuffix(s.Name, ".md")
		neighbors = append(neighbors, Location{
			Display: name,
			URL:     name + ".html",
		})
	}
	return neighbors, nil
}

// copyFile copies an input file from inDir to the outDir.
func copyFile(input, inDir, outDir string) error {
	data, err := os.ReadFile(input)
	if err != nil {
		return err
	}

	path := strings.TrimPrefix(input, filepath.Clean(inDir))
	path = filepath.Join(outDir, path)
	if err := os.MkdirAll(filepath.Dir(path), 0755); err != nil {
		return err
	}
	return os.WriteFile(path, data, 0644)
}
