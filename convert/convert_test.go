package convert

import (
	"reflect"
	"testing"
)

func TestPathBar(t *testing.T) {
	type test struct {
		path   string
		outDir string
		want   []Location
	}

	tests := []test{
		{
			path:   "a/b/c/d.html",
			outDir: "a/b/c",
			want: []Location{
				{
					Display: "~/",
					URL:     "/",
				},
				{
					Display: "d",
					URL:     "/d.html",
				},
			},
		},
	}

	for _, tc := range tests {
		got := pathBar(tc.path, tc.outDir)
		if !reflect.DeepEqual(tc.want, got) {
			t.Fatalf("expected: %v, got: %v", tc.want, got)
		}
	}
}

func TestOutputPath(t *testing.T) {
	type test struct {
		input  string
		inDir  string
		outDir string
		want   string
	}

	tests := []test{
		{
			input:  "cmd/adelaii.de/testdata/in/audio/field/index.md",
			inDir:  "./cmd/adelaii.de/testdata/in",
			outDir: "./cmd/adelaii.de/testdata/out",
			want:   "cmd/adelaii.de/testdata/out/audio/field/index.html",
		},
	}

	for _, tc := range tests {
		got := outputPath(tc.input, tc.inDir, tc.outDir)
		if got != tc.want {
			t.Fatalf("expected: %v, got: %v", tc.want, got)
		}
	}
}
