package visible

import (
	"bufio"
	"bytes"
	"errors"
	"os"
	"path/filepath"
	"strings"
)

const VisibleKeyword = "VISIBLE"

// Is returns true if file at path is meant to be visible.
// If a file contains the VisibleKeyword on line one, it will be visible,
// directories will be considered visible if their index.md file is visible.
func Is(path string, isDir bool) (bool, error) {
	// Use the index.md of a dir instead of the dir itself.
	if isDir {
		path = filepath.Join(path, "index.md")
	}

	// Non-markdown files are not visible.
	if !strings.HasSuffix(path, ".md") {
		return false, nil
	}

	f, err := os.Open(path)
	if errors.Is(err, os.ErrNotExist) {
		// If the dir didn't contain an index.md it is not an error, but is not
		// visible.
		return false, nil
	} else if err != nil {
		return false, err
	}
	defer f.Close()

	scanner := bufio.NewScanner(f)

	for scanner.Scan() {
		if scanner.Text() == VisibleKeyword {
			return true, nil
		}
	}
	return false, nil
}

// Strip returns a copy of slice s without lines containing the VisibleKeyword.
func Strip(s []byte) []byte {
	var buf bytes.Buffer
	var line bytes.Buffer
	for _, c := range s {
		switch c {
		case '\n':
			l := line.String()
			if l != VisibleKeyword {
				line.WriteByte(c)
				buf.WriteString(line.String())
			}
			line.Reset()
		default:
			line.WriteByte(c)
		}
	}
	return buf.Bytes()
}
