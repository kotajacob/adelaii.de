package main

import (
	"fmt"
	"os"

	"git.sr.ht/~kota/adelaii.de/convert"
	"git.sr.ht/~kota/adelaii.de/static"
	"git.sr.ht/~kota/adelaii.de/walk"
)

func usage() {
	fmt.Fprintln(os.Stderr, "usage: adelaii.de [input_path] [output_path]")
	os.Exit(1)
}

func main() {
	if len(os.Args) != 3 {
		usage()
	}
	inDir, outDir := os.Args[1], os.Args[2]

	tree, err := walk.Walk(inDir)
	if err != nil {
		fmt.Fprintf(
			os.Stderr,
			"Failed while walking the filetree: %v\n",
			err,
		)
	}

	err = convert.All(tree, inDir, outDir)
	if err != nil {
		fmt.Fprintf(
			os.Stderr,
			"Failed while converting files: %v\n",
			err,
		)
	}

	err = static.Copy(outDir)
	if err != nil {
		fmt.Fprintf(
			os.Stderr,
			"Failed while copying static files: %v\n",
			err,
		)
	}
}
