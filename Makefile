.POSIX:

include config.mk

all: clean build

build:
	go build ./cmd/adelaii.de/ $(GOFLAGS)

test:
	fd -e go | entr -c go test ./...

clean:
	rm -f adelaii.de

install: build
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	cp -f adelaii.de $(DESTDIR)$(PREFIX)/bin
	chmod 755 $(DESTDIR)$(PREFIX)/bin/adelaii.de

uninstall:
	rm -f $(DESTDIR)$(PREFIX)/bin/adelaii.de

.PHONY: all build upload test clean install uninstall
