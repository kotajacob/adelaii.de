package walk

import (
	"bytes"
	"math/rand"
	"os"
	"path/filepath"
	"strconv"
	"testing"
	"time"
)

func TestWalk(t *testing.T) {
	dir := t.TempDir()
	rng := rand.New(rand.NewSource(time.Now().Unix()))
	if err := generateWiki(rng, dir, 0); err != nil {
		t.Fatal(err)
	}

	got, err := Walk(dir)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(got)
}

var letters = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

func generateWord(rng *rand.Rand, characters int) string {
	b := make([]rune, characters)
	for i := range b {
		b[i] = letters[rng.Intn(len(letters))]
	}
	return string(b)
}

func generateParagraph(rng *rand.Rand, words int) string {
	var b bytes.Buffer
	b.WriteString(generateWord(rng, rng.Intn(8)+2))
	for i := 1; i < words; i++ {
		b.WriteString(" ")
		b.WriteString(generateWord(rng, rng.Intn(8)+2))
	}
	return b.String()
}

func generateMarkdown(rng *rand.Rand, paragraphs int) []byte {
	var b bytes.Buffer
	b.WriteString(generateParagraph(rng, rng.Intn(150)+50))
	for i := 1; i < paragraphs; i++ {
		b.WriteString("\n\n")
		b.WriteString(generateParagraph(rng, rng.Intn(100)+100))
	}
	return b.Bytes()
}

func generateWiki(rng *rand.Rand, dir string, depth int) error {
	for i := 0; i < 2; i++ {
		path := filepath.Join(dir, strconv.Itoa(i))
		if err := os.MkdirAll(path, 0777); err != nil {
			return err
		}
		if depth < 3 {
			if err := generateWiki(rng, path, depth+1); err != nil {
				return err
			}
		}
	}
	for i := 0; i < 5; i++ {
		if err := os.WriteFile(
			filepath.Join(dir, strconv.Itoa(i)+".md"),
			generateMarkdown(rng, rng.Intn(6)+4),
			0666,
		); err != nil {
			return err
		}
	}
	return nil
}
