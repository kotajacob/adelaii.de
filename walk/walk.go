package walk

import (
	"fmt"
	"os"
	"path/filepath"
	"strconv"
	"strings"

	"git.sr.ht/~kota/adelaii.de/visible"
)

type Node struct {
	Name      string
	Path      string
	IsDir     bool
	IsVisible bool
	Parent    *Node
	Children  []*Node
}

// Walk the filetree and build a node tree.
func Walk(path string) (*Node, error) {
	// Create the root node.
	stat, err := os.Stat(path)
	if err != nil {
		return &Node{}, err
	}
	if !stat.IsDir() {
		return &Node{}, fmt.Errorf("%v is not a directory", path)
	}
	root := Node{
		Name:      filepath.Base(path),
		Path:      path,
		IsDir:     true,
		IsVisible: false,
	}

	return &root, walkDir(&root)
}

// walkDir recursively walks the file tree rooted at dir and sends the children
// of the parent node on the children channel.
func walkDir(parent *Node) error {
	entries, err := os.ReadDir(parent.Path)
	if err != nil {
		return err
	}
	for _, entry := range entries {
		if strings.HasPrefix(entry.Name(), ".") {
			// Skip system hidden files and directories.
			continue
		}
		if entry.IsDir() {
			path := filepath.Join(parent.Path, entry.Name())
			visible, err := visible.Is(path, true)
			if err != nil {
				return err
			}
			subdir := Node{
				Name:      entry.Name(),
				Path:      path,
				IsDir:     true,
				IsVisible: visible,
				Parent:    parent,
			}
			parent.Children = append(parent.Children, &subdir)
			walkDir(&subdir)
		} else {
			path := filepath.Join(parent.Path, entry.Name())
			visible, err := visible.Is(path, false)
			if err != nil {
				return err
			}
			child := Node{
				Name:      entry.Name(),
				Path:      path,
				IsDir:     false,
				IsVisible: visible,
				Parent:    parent,
			}
			parent.Children = append(parent.Children, &child)
		}
	}
	return nil
}

// Next is used to iterate through the tree of nodes starting with n.
// This is acheived by traversing down, across, and then back up.
func (n *Node) Next() *Node {
	if len(n.Children) != 0 {
		// We descend deeper. We always go to the first child, since moving to
		// the next sibling is handled below.
		return n.Children[0]
	}

	// Find the next sibling.
	for {
		if n.Parent == nil {
			return nil
		}
		for i, sibling := range n.Parent.Children {
			// Find ourselves.
			if sibling.Name == n.Name {
				if i < len(n.Parent.Children)-1 {
					return n.Parent.Children[i+1]
				} else {
					break
				}
			}
		}
		n = n.Parent
	}
}

// Root climbs the tree to return the eldest granparent of node.
func (n *Node) Root() *Node {
	for n.Parent != nil {
		n = n.Parent
	}
	return n
}

// Siblings returns a list of nodes in the same directory as n.
func (n *Node) Siblings() []*Node {
	var siblings []*Node
	for _, s := range n.Parent.Children {
		if s == n {
			continue
		}
		siblings = append(siblings, s)
	}
	return siblings
}

// String implements the fmt.String interface by returning a string
// representation of the Node tree.
func (n Node) String() string {
	np := n.Root()
	var b strings.Builder
	for np != nil {
		b.WriteString("Path: ")
		b.WriteString(np.Path)
		b.WriteString("\n")

		b.WriteString("IsDir: ")
		b.WriteString(strconv.FormatBool(np.IsDir))
		b.WriteString("\n")

		b.WriteString("IsVisible: ")
		b.WriteString(strconv.FormatBool(np.IsVisible))
		b.WriteString("\n")
		np = np.Next()
	}
	return b.String()
}
